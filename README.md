Audiology Clinics of Puerto Rico was established in 1976 to provide diagnostic evaluations and rehabilitation of hearing and balance disorders. We are now the largest center in the Caribbean, providing diagnostic evaluations and rehabilitation in the southern, western and northwestern coasts of PR.

Address: Medicos DeDiego Este #14, Suite 206, 00680 Mayaguez, Puerto Rico

Phone: +1 787-834-0660